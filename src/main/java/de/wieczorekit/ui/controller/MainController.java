/*
 * 18.04.2014 | 14:09:35
 * Marcel Wieczorek
 * marcel@wieczorek-it.de
 */
package de.wieczorekit.ui.controller;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import de.wieczorekit.ant.Level;
import de.wieczorekit.ant.Player;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 * @since 1.0
 */
public class MainController implements Initializable {

    @FXML
    private ImageView imageView;

    /*
     * (non-Javadoc)
     * @see javafx.fxml.Initializable#initialize(java.net.URL, java.util.ResourceBundle)
     */
    @Override
    public void initialize(final URL location, final ResourceBundle resources) {
        Platform.runLater(() -> new Player(imageView).play(Level.RECTANGLE));
    }

}
