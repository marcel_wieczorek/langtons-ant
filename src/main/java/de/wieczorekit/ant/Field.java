/**
 * langtons-ant
 * 06.05.2012 | 12:34:24
 */
package de.wieczorekit.ant;

/**
 * @author <a href="mailto:marcel@wieczorek-it.de"><b>Marcel Wieczorek</b></a>
 */
public class Field {

    private boolean black;

    public void toggleColor() {
        black = !black;
    }

    public boolean isBlack() {
        return black;
    }

    public void setBlack(final boolean black) {
        this.black = black;
    }

}
