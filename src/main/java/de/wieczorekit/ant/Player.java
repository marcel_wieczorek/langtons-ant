/*
 * 18.04.2014 | 14:20:15
 * Marcel Wieczorek
 * marcel@wieczorek-it.de
 */
package de.wieczorekit.ant;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 * @since 1.0
 */
public class Player {

    private final ImageView imageView;

    private List<List<Field>> fields2d;
    private Ant ant;
    private WritableImage image;

    public Player(final ImageView imageView) {
        Objects.requireNonNull(imageView, "imageView must not be null");

        this.imageView = imageView;
    }

    public void play(final Level level) {
        initialize(level);

        final Timeline timeLine = new Timeline();
        timeLine.setCycleCount(Timeline.INDEFINITE);

        timeLine.getKeyFrames().add(new KeyFrame(Duration.millis(.1), (final ActionEvent t) -> {
            try {
                Field field = fields2d.get(ant.getPosition().x).get(ant.getPosition().y);
                field.toggleColor();

                final PixelWriter pixelWriter = image.getPixelWriter();
                pixelWriter.setColor(ant.getPosition().x, ant.getPosition().y, field.isBlack() ? Color.BLACK : Color.WHITE);

                ant.run();

                field = fields2d.get(ant.getPosition().x).get(ant.getPosition().y);

                if (field.isBlack()) {
                    ant.turnLeft();
                } else {
                    ant.turnRight();
                }
            } catch (final IndexOutOfBoundsException e) {
                timeLine.stop();
            }
        }, new KeyValue[0]) // don't use binding
        );

        timeLine.playFromStart();
    }

    protected void initialize(final Level level) {
        fields2d = level.load((int) imageView.getFitWidth(), (int) imageView.getFitHeight());
        ant = new Ant(fields2d.size() / 2, fields2d.get(fields2d.size() / 2).size() / 2);

        image = new WritableImage((int) imageView.getFitWidth(), (int) imageView.getFitHeight());
        final PixelWriter pixelWriter = image.getPixelWriter();

        final AtomicInteger x = new AtomicInteger(0);
        final AtomicInteger y = new AtomicInteger(0);
        fields2d.forEach(fields -> {
            y.set(0);

            fields.forEach(field -> {
                pixelWriter.setColor(x.get(), y.get(), field.isBlack() ? Color.BLACK : Color.WHITE);

                y.incrementAndGet();
            });

            x.incrementAndGet();
        });

        imageView.setImage(image);
    }

}
