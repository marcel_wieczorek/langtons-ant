/**
 * langtons-ant
 * 06.05.2012 | 12:30:35
 */
package de.wieczorekit.ant;

import java.awt.Point;

/**
 * @author <a href="mailto:marcel@wieczorek-it.de"><b>Marcel Wieczorek</b></a>
 */
public class Ant {

    private final Point position = new Point();
    private Direction direction = Direction.EAST;

    public Ant() {
        // do nothing
    }

    public Ant(final int x, final int y) {
        position.setLocation(x, y);
    }

    /**
     * @return the position
     */
    public Point getPosition() {
        return position;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format("Ant [position=%s, direction=%s]", position, direction);
    }

    public void run() {
        direction.run(position);
    }

    public void turnLeft() {
        direction = direction.toLeft();
    }

    public void turnRight() {
        direction = direction.toRight();
    }

}
