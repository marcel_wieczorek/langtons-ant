<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>de.wieczorekit</groupId>
	<artifactId>langonts-ant</artifactId>
	<version>1.0.0</version>
	<name>langtons-ant</name>
	<description><![CDATA[An implementation of Langton's ant.
			Langton's ant is a two-dimensional Turing machine with a very simple set of rules but complicated emergent behavior.
			It was invented by Chris Langton in 1986 and runs on a square lattice of black and white cells.
		]]></description>
	<packaging>jar</packaging>
	<prerequisites>
		<maven>3.0</maven>
	</prerequisites>
	<organization>
		<name>Marcel Wieczorek IT-Services</name>
		<url>http://www.wieczorek-it.de</url>
	</organization>
	<!-- SOURCE CONTROL MANAGEMENT -->
	<scm>
		<connection>scm:git:https://bitbucket.org/marcel_wieczorek/${project.artifactId}.git</connection>
		<url>http://code.wieczorek-it.de/${project.artifactId}</url>
	</scm>
	<!-- DISTRIBUTION MANAGEMENT -->
	<distributionManagement>
		<site>
       		<id>${project.artifactId}-site</id>
       		<url>${project.baseUri}</url>
     	</site>
	</distributionManagement>
	<!-- DEVELOPERS -->
	<developers>
		<developer>
			<id>mwieczorek</id>
			<name>Marcel Wieczorek</name>
			<email>marcel@wieczorek-it.de</email>
			<organization>Marcel Wieczorek IT-Services</organization>
			<organizationUrl>http://www.wieczorek-it.de</organizationUrl>
			<roles>
				<role>architect</role>
				<role>software engineer</role>
			</roles>
		</developer>
	</developers>
	<!-- PROPERTIES -->
	<properties>
		<junit.version>4.11</junit.version>
		<project.compiler.source.version>1.8</project.compiler.source.version>
		<project.compiler.target.version>1.8</project.compiler.target.version>
		<project.build.sourceEncoding>ISO-8859-1</project.build.sourceEncoding>
		<maven.build.timestamp.format>EEE MMM dd HH:mm:ss zzz yyyy</maven.build.timestamp.format>
		<slf4j.version>1.7.5</slf4j.version>
		<de.wieczorekit.build.timestamp>${maven.build.timestamp}</de.wieczorekit.build.timestamp>
		<de.wieczorekit.build.revision>${buildNumber}</de.wieczorekit.build.revision>
	</properties>
	<!-- DEPENDENCIES -->
	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>${junit.version}</version>
				<scope>test</scope>
			</dependency>
			<!--  -->
			<dependency>
				<groupId>org.slf4j</groupId>
				<artifactId>slf4j-log4j12</artifactId>
				<version>${slf4j.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>
	<!-- BUILD -->
	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-clean-plugin</artifactId>
					<version>2.4.1</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.1</version>
					<configuration>
						<source>1.8</source>
						<target>1.8</target>
						<encoding>${project.build.sourceEncoding}</encoding>
					</configuration>
				</plugin>
				<plugin>
			        <groupId>org.apache.maven.plugins</groupId>
			        <artifactId>maven-jar-plugin</artifactId>
			        <version>2.4</version>
        		</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-deploy-plugin</artifactId>
					<version>2.7</version>
				</plugin>
			</plugins>
		</pluginManagement>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-clean-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.12.4</version>
				<configuration>
					<excludes>
						<exclude>**/*Test.java</exclude>
					</excludes>
					<forkMode>always</forkMode>
				</configuration>
			</plugin>
			<plugin>
				<groupId>com.zenjava</groupId>
				<artifactId>javafx-maven-plugin</artifactId>
				<version>2.0</version>
				<configuration>
					<mainClass>de.wieczorekit.Main</mainClass>

					<!-- only required if signing the jar file -->
					<keyStoreAlias>n-expert-ks</keyStoreAlias>
					<keyStorePassword>changeit</keyStorePassword>
					<permissions>
						<permission>all-permissions</permission>
					</permissions>
				</configuration>
				<executions>
					<execution>
						<phase>install</phase>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-site-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<reportPlugins>
						<plugin>
							<groupId>org.apache.maven.plugins</groupId>
							<artifactId>maven-project-info-reports-plugin</artifactId>
							<version>2.4</version>
							<configuration>
								<dependencyDetailsEnabled>false</dependencyDetailsEnabled>
								<dependencyLocationsEnabled>false</dependencyLocationsEnabled>
							</configuration>
							<!-- simpler configuration without reportSets available for usual cases -->
							<reports>
								<report>index</report>
								<report>project-team</report>
								<report>dependencies</report>
								<report>license</report>
								<report>scm</report>
							</reports>
						</plugin>
						<plugin>
							<groupId>org.apache.maven.plugins</groupId>
							<artifactId>maven-pmd-plugin</artifactId>
							<version>2.7.1</version>
							<configuration>
								<targetJdk>1.8</targetJdk>
							</configuration>
						</plugin>
						<plugin>
							<groupId>org.codehaus.mojo</groupId>
							<artifactId>findbugs-maven-plugin</artifactId>
							<version>2.5.1</version>
						</plugin>
						<plugin>
							<groupId>org.apache.maven.plugins</groupId>
							<artifactId>maven-javadoc-plugin</artifactId>
							<version>2.8</version>
						</plugin>
						<plugin>
							<groupId>org.apache.maven.plugins</groupId>
							<artifactId>maven-surefire-report-plugin</artifactId>
							<version>2.12</version>
						</plugin>
					</reportPlugins>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-deploy-plugin</artifactId>
			</plugin>
		</plugins>
	</build>
</project>