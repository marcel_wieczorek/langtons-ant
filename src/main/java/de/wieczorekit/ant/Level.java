/**
 *
 */
package de.wieczorekit.ant;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:marcel@wieczorek-it.de"><b>Marcel Wieczorek</b></a>
 */
public enum Level {

    /** */
    DEFAULT {
        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.ELevel#load(int, int)
         */
        @Override
        public List<List<Field>> load(final int width, final int height) {
            final List<List<Field>> result = new ArrayList<>(width);

            for (int x = 0; x < width; x++) {
                final List<Field> rows = new ArrayList<>(height);

                for (int y = 0; y < height; y++) {
                    rows.add(new Field());
                }

                result.add(rows);
            }

            return result;
        }
    },

    /** */
    CROSS {
        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.ELevel#load(int, int)
         */
        @Override
        public List<List<Field>> load(final int width, final int height) {
            final List<List<Field>> result = new ArrayList<>(width);

            final int minX = width / 4;
            final int maxX = minX * 2;

            final int minY = height / 4;
            final int maxY = minY * 2;

            for (int x = 0; x < width; x++) {
                final List<Field> rows = new ArrayList<>(height);

                for (int y = 0; y < height; y++) {
                    final Field field = new Field();

                    rows.add(field);

                    if (x >= minX && x <= maxX || y >= minY && y <= maxY) {
                        if (x % 13 == 0 || y % 13 == 0) {
                            field.toggleColor();
                        }
                    }
                }

                result.add(rows);
            }

            return result;
        }
    },

    /** */
    RECTANGLE {
        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.ELevel#load(int, int)
         */
        @Override
        public List<List<Field>> load(final int width, final int height) {
            final List<List<Field>> result = new ArrayList<>(width);

            final int minXleft = height / 3;
            final int maxXleft = minXleft + 30;

            final int minXright = width - minXleft;

            final int minYleft = height / 3;
            final int maxYleft = minYleft + 30;

            final int minYright = height - minYleft;

            for (int x = 0; x < width; x++) {
                final List<Field> rows = new ArrayList<>(height);

                for (int y = 0; y < height; y++) {
                    final Field field = new Field();

                    rows.add(field);

                    if (x <= maxXleft || x >= minXright) {
                        field.toggleColor();
                    } else if (y <= maxYleft || y >= minYright) {
                        field.toggleColor();
                    }
                }

                result.add(rows);
            }

            return result;
        }
    };

    public abstract List<List<Field>> load(int width, int height);

}
