/**
 *
 */
package de.wieczorekit.ant;

import java.awt.Point;

/**
 * @author <a href="mailto:marcel@wieczorek-it.de"><b>Marcel Wieczorek</b></a>
 */
public enum Direction {
    /** */
    NORTH {
        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toLeft()
         */
        @Override
        public Direction toLeft() {
            return WEST;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toRight()
         */
        @Override
        public Direction toRight() {
            return EAST;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#run(java.awt.Point)
         */
        @Override
        public void run(final Point point) {
            point.y--;
        }
    },

    /** */
    EAST {
        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toLeft()
         */
        @Override
        public Direction toLeft() {
            return NORTH;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toRight()
         */
        @Override
        public Direction toRight() {
            return SOUTH;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#run(java.awt.Point)
         */
        @Override
        public void run(final Point point) {
            point.x++;
        }
    },

    /** */
    SOUTH {
        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toLeft()
         */
        @Override
        public Direction toLeft() {
            return EAST;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toRight()
         */
        @Override
        public Direction toRight() {
            return WEST;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#run(java.awt.Point)
         */
        @Override
        public void run(final Point point) {
            point.y++;
        }
    },

    /** */
    WEST {
        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toLeft()
         */
        @Override
        public Direction toLeft() {
            return SOUTH;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#toRight()
         */
        @Override
        public Direction toRight() {
            return NORTH;
        }

        /*
         * (non-Javadoc)
         * @see de.wieczorekit.ant.EDirection#run(java.awt.Point)
         */
        @Override
        public void run(final Point point) {
            point.x--;
        }
    };

    public abstract Direction toLeft();

    public abstract Direction toRight();

    public abstract void run(Point point);
}
