/*
 * 18.04.2014 | 13:46:33
 * Marcel Wieczorek
 * marcel@wieczorek-it.de
 */
package de.wieczorekit;

import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author Marcel Wieczorek
 * @version 1.0
 * @since 1.0
 */
public class Main extends Application {

    /**
     * @param args
     * @author Marcel Wieczorek
     * @since 1.0
     */
    public static void main(final String... args) {
        launch(args);
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        final URL location = Main.class.getResource("/de/wieczorekit/ui/fxml/Main.fxml");

        final FXMLLoader fxmlLoader = new FXMLLoader(location);
        final StackPane stackPane = (StackPane) fxmlLoader.load();
        final Scene scene = new Scene(stackPane);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

}
